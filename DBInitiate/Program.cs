﻿using System;
using System.IO;
using AOPAspects;
using ConnectionConfigService;

namespace DBInitiate
{
    public static class Program {

        public static void Main(string[] args) {
            try
            {
                AopAspectConfigure.Configure(Interaction.TraceMessageWrite, Interaction.TraceMessageWrite);
                Interaction.Configure();

                Interaction.MessageWrite("Connection string verification start");
                var config = ConnectionConfigBuilder.Build();
                Interaction.MessageWrite("Connection string verification completed");

                Interaction.MessageWrite("Initiate start");
                DbService.TestMasterConnection(config);
                DbInitiatiator.Initiate(config);
                Interaction.MessageWrite("Initiate completed");

                Interaction.MessageWrite("Update start");
                UpdateService.UpdateApply(config);
                Interaction.MessageWrite("Update complited");

                if (config.IsLocal && Interaction.Query("Open folder contains localDB file?") == Interaction.QuaryResult.Yes)
                    ExplorerService.OpenFolder(Path.GetDirectoryName(config.LocalDbFilePath));
            }
            catch (Exception ex)
            {
                Interaction.ExceptionWrite(ex.Message);
            }
            finally
            {
                Interaction.MessageWrite("Press any key to exit");
                Console.ReadKey(true);
            }
        }
    }
}
