﻿using System.ComponentModel;

namespace DBInitiate
{
    internal enum DbInitiatiateState
    {
        [Description("DB init success")]
        Success,
        [Description("Dbms {0} not exist")]
        DbmsNotExist,
    }
}
