﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBInitiate
{
    internal static class ExplorerService
    {
        internal static bool OpenFolder(string path) {
            try {
                Process.Start(path);
                return true;
            }
            catch {
                return false;
            }
        }
    }
}
