﻿using AOPAspects.MethodTrace;
using AspectInjector.Broker;
using ConnectionConfigService;

namespace DBInitiate
{
    [Aspect(typeof(TraceMethodStartAspect))]
    [Aspect(typeof(TraceMethodExitAspect))]
    internal static class DbInitiatiator
    {
        internal static void Initiate(ConnectionConfig connectionConfig)
        {
            if (DbService.TryConnection(connectionConfig) ||
                DbService.MasterHasDbEntry(connectionConfig)) {

                if (Interaction.Query($"Target Db {connectionConfig.LocalDbFilePathCommon} exist. Overwrite?") == Interaction.QuaryResult.No)
                    return;

                DbService.Drop(connectionConfig);
            }

            DbService.Create(connectionConfig);
            DbService.ActivateDataBase(connectionConfig);
        }
    }
}
