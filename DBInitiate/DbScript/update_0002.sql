﻿create trigger dbo.trg_TaskTemplate_Insert
on dbo.TaskTemplate
for insert
as
begin
declare @currentDate DateTime
set @currentDate = GetDate()
update 
	dbo.TaskTemplate
set 
	_LastUpdate = @currentDate,
	_Create = @currentDate
from 
	dbo.TaskTemplate tt join inserted i on tt.Id = i.Id
end
GO
create trigger dbo.trg_TaskTemplate_Update
on dbo.TaskTemplate
for update
as
begin
if trigger_nestlevel() > 1
	return

update
	dbo.TaskTemplate
set
	_LastUpdate = GetDate()
from 
	dbo.TaskTemplate tt join inserted i on tt.Id = i.Id
end
GO
create trigger dbo.trg_TaskTemplate_Delete
on dbo.TaskTemplate
instead of delete
as
begin
update dbo.TaskTemplate
set _Delete = GetDate(),
_IsDelete = 1
from 
	dbo.TaskTemplate tt join deleted d on tt.Id = d.Id
end
GO
create trigger dbo.trg_TaskTimeDetails_Insert
on dbo.TaskTimeDetails
for insert
as
begin
declare @currentDate DateTime
set @currentDate = GetDate()
update 
	dbo.TaskTimeDetails
set 
	_LastUpdate = @currentDate,
	_Create = @currentDate
from 
	dbo.TaskTimeDetails tt join inserted i on tt.Id = i.Id
end
GO
create trigger dbo.trg_TaskTimeDetails_Update
on dbo.TaskTimeDetails
for update
as
begin
if trigger_nestlevel() > 1
	return
update
	dbo.TaskTimeDetails
set
	_LastUpdate = GetDate()
from 
	dbo.TaskTimeDetails tt join inserted i on tt.Id = i.Id
end
GO
create trigger dbo.trg_TaskTimeDetails_Delete
on dbo.TaskTimeDetails
instead of delete
as
begin
update dbo.TaskTimeDetails
set _Delete = GetDate(),
_IsDelete = 1
from 
	dbo.TaskTimeDetails tt join deleted d on tt.Id = d.Id
end
GO
create trigger dbo.trg_Periodicity_Insert
on dbo.Periodicity
for insert
as
begin
declare @currentDate DateTime
set @currentDate = GetDate()
update 
	dbo.Periodicity
set 
	_LastUpdate = @currentDate,
	_Create = @currentDate
from 
	dbo.Periodicity tt join inserted i on tt.Id = i.Id
end
GO
create trigger dbo.trg_Periodicity_Update
on dbo.Periodicity
for update
as
begin
if trigger_nestlevel() > 1
	return
update
	dbo.Periodicity
set
	_LastUpdate = GetDate()
from 
	dbo.Periodicity tt join inserted i on tt.Id = i.Id
end
GO
create trigger dbo.trg_Periodicity_Delete
on dbo.Periodicity
instead of delete
as
begin
update dbo.Periodicity
set _Delete = GetDate(),
_IsDelete = 1
from 
	dbo.Periodicity tt join deleted d on tt.Id = d.Id
end
GO
create trigger dbo.trg_TaskItem_Insert
on dbo.TaskItem
for insert
as
begin
declare @currentDate DateTime
set @currentDate = GetDate()
update 
	dbo.TaskItem
set 
	_LastUpdate = @currentDate,
	_Create = @currentDate
from 
	dbo.TaskItem tt join inserted i on tt.Id = i.Id
end
GO
create trigger dbo.trg_TaskItem_Update
on dbo.TaskItem
for update
as
begin
if trigger_nestlevel() > 1
	return
update
	dbo.TaskItem
set
	_LastUpdate = GetDate()
from 
	dbo.TaskItem tt join inserted i on tt.Id = i.Id
end
GO
create trigger dbo.trg_TaskItem_Delete
on dbo.TaskItem
instead of delete
as
begin
update dbo.TaskItem
set _Delete = GetDate(),
_IsDelete = 1
from 
	dbo.TaskItem tt join deleted d on tt.Id = d.Id
end