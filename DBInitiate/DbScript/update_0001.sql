﻿create type UserDescription from NVarChar(256) null
GO
create type DictionaryDescription from NVarChar(16) not null
GO
create table dbo.TaskTemplate
(
Id int identity(1,1) not null,
Name NVarChar(64) not null,
[Description] UserDescription null,
UserId int not null,
_Create DateTime null,
_LastUpdate DateTime null,
_Delete DateTime null,
_IsDelete bit default(0)
)
GO
create table dbo.TaskTimeDetails
(
Id int identity(1,1) not null,
StartTime int null,
EndTime int null,
DurationMin int null,
DurationMax int null,
TaskTimeBoundTypeId int not null,
TaskTemplateId int not null,
_Create DateTime null,
_LastUpdate DateTime null,
_Delete DateTime null,
_IsDelete bit default(0)
)
GO
create table dbo.TaskTimeBoundTypeDic
(
Id int identity(1,1),
[Description] DictionaryDescription
)
GO
create table dbo.Periodicity
(
Id int identity(1,1) not null,
TaskTemplateId int not null,
RepeateCount int default 1 not null,
PeriodUnitTypeId int not null,
StartPosition smallint not null,
[Timeout] int default 0 not null,
_Create DateTime null,
_LastUpdate DateTime null,
_Delete DateTime null,
_IsDelete bit default(0)
)
GO
create table dbo.PeriodUnitTypeDic
(
Id int identity(1,1) not null,
[Description] DictionaryDescription
)
GO
create table dbo.TaskItem
(
Id int identity(1,1) not null,
TaskTemplateId int not null,
TaskTimeDetailsId int not null,
TaskStatusTypeId int not null,
[Description] UserDescription,
_Create DateTime null,
_LastUpdate DateTime null,
_Delete DateTime null,
_IsDelete bit default(0)
)
GO
create table dbo.TaskStatusTypeDic
(
Id int identity(1,1) not null,
[Description] DictionaryDescription
)
GO
alter table dbo.TaskTemplate add constraint PK_TaskTemplate primary key (Id)
GO
alter table dbo.TaskTimeDetails add constraint PK_TaskTimeDetails primary key (Id)
GO
alter table dbo.TaskTimeBoundTypeDic add constraint PK_TaskTimeBoundTypeDic primary key (Id)
GO
alter table dbo.Periodicity add constraint PK_Periodicity primary key (Id)
GO
alter table dbo.TaskItem add constraint PK_TaskItem primary key (Id)
GO
alter table dbo.TaskStatusTypeDic add constraint PK_TaskStatusTypeDic primary key (Id)
GO
alter table dbo.PeriodUnitTypeDic add constraint PK_PeriodUnitTypeDic primary key (Id)
GO
alter table dbo.TaskTimeDetails
add constraint FK_TaskTimeDetails_TaskTemplate foreign key (TaskTemplateId) references dbo.TaskTemplate(Id)
GO
alter table dbo.TaskTimeDetails
add constraint FK_TaskTimeDetails_TaskTimeBoundTypeDic foreign key (TaskTimeBoundTypeId) references dbo.TaskTimeBoundTypeDic(Id)
GO
alter table dbo.Periodicity
add constraint FK_Periodicity_TaskTemplate foreign key (TaskTemplateId) references dbo.TaskTemplate(Id)
GO
alter table dbo.Periodicity
add constraint FK_Periodicity_PeriodUnitTypeDic foreign key (PeriodUnitTypeId) references dbo.PeriodUnitTypeDic(Id)
GO
alter table dbo.TaskItem
add constraint FK_TaskItem_TaskTemplate foreign key (TaskTemplateId) references dbo.TaskTemplate(Id)
GO
alter table dbo.TaskItem
add constraint FK_TaskItem_TaskTimeDetails foreign key (TaskTimeDetailsId) references dbo.TaskTimeDetails(Id)
GO
alter table dbo.TaskItem
add constraint FK_TaskItem_TaskStatusTypeDic foreign key (TaskStatusTypeId) references dbo.TaskStatusTypeDic(Id)
GO
set identity_insert dbo.TaskTimeBoundTypeDic on
GO
insert into dbo.TaskTimeBoundTypeDic(Id, [Description]) values (0, N'Process'), (1, N'Start'), (2, N'Target'), (3,N'AnyTime'), (4,N'ExactlyWithin')
GO
set identity_insert dbo.TaskTimeBoundTypeDic off

GO
set identity_insert dbo.PeriodUnitTypeDic on
GO
insert into dbo.PeriodUnitTypeDic(Id, [Description]) values (0, N'Day'), (1, N'WeekDay'), (2, N'MonthDay')
GO
set identity_insert dbo.PeriodUnitTypeDic off

GO
set identity_insert dbo.TaskStatusTypeDic on
GO
insert into dbo.TaskStatusTypeDic(Id, [Description]) values (0, N'Planned'), (1, N'InProgress'), (2, N'Complite'), (3, N'Cancel'), (4, N'Skipped')
GO
set identity_insert dbo.TaskStatusTypeDic off