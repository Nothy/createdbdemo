﻿using System;

namespace DBInitiate
{
    internal static class Interaction
    {
        internal static void Configure() {
            Console.BackgroundColor = ConsoleColor.White;
            Console.Clear();
        }

        internal static void ExceptionWrite(string exceptionMessage)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(exceptionMessage);
        }

        internal static void MessageWrite(string message)
        {
            Console.ForegroundColor = ConsoleColor.Black;
            Console.WriteLine(message);
        }

        internal static void SuccessMessageWrite(string message)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(message);
        }

        internal static void TraceMessageWrite(string message)
        {
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine(message);
        }

        internal enum QuaryResult {
            Yes,
            No
        }

        internal static QuaryResult Query(string question) {
            Console.ForegroundColor = ConsoleColor.Blue;
            
            while (true) {
                Console.WriteLine($"{question} (Y/N)");
                var keyInput = Console.ReadKey(true);

                switch (keyInput.Key) {
                    case ConsoleKey.Y:
                        return QuaryResult.Yes;
                    case ConsoleKey.N:
                        return QuaryResult.No;
                }
            }
        }
    }
}
