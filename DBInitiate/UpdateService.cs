﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using AOPAspects.MethodTrace;
using AspectInjector.Broker;
using ConnectionConfigService;

namespace DBInitiate
{
  [Aspect(typeof(TraceMethodStartAspect))]
  [Aspect(typeof(TraceMethodExitAspect))]
  internal static class UpdateService
  {
    private const string UPDATE_FILES_SEACH_PATTERN = @"^update_[\d][\d][\d][\d].sql";
    private const string CHECK_VERSIONS_LOG_EXISTS_QUERY = "Select Object_Id('app.DbVersionsLog')";
    private const string CHECK_SCHEMA_APP_EXISTS_QUERY = "Select Schema_Id('app')";
    private const string CREATE_SCHEMA_COMMAND = "Create schema app";
    private const string CREATE_VERSIONS_LOG_COMMAND = "Create table app.DbVersionsLog(Version int, ApplayDate DateTime, FileName nvarchar(64))";
    private const string SELECT_MAX_VERSION_QUERY = "Select max(Version) from app.DbVersionsLog";
    private const string SAVE_ABOUT_VERSION_COMMAND = "Insert into app.DbVersionsLog(Version, ApplayDate, FileName) values (@Version, @ApplayDate, @FileName)";

    private static readonly string[] batchSplitter = { "GO", "Go", "go" };

    internal static void UpdateApply(ConnectionConfig connectionConfig)
    {
      var updateFilesList = GetUpdateFileEntries();
      var lastUpdateVersion = GetLastUpdateVersion(connectionConfig);
      var newUpdateFilesList = updateFilesList.Where(f => f.Version > lastUpdateVersion);
      SetUpdateToDataBase(connectionConfig, newUpdateFilesList);
    }

    private static int GetLastUpdateVersion(ConnectionConfig connectionConfig)
    {
      using (var connection = new SqlConnection(connectionConfig.SqlConnectionBuilder.ConnectionString))
      {
        connection.Open();

        if (VersionsLogExist(connectionConfig))
          using (var query = new SqlCommand(SELECT_MAX_VERSION_QUERY, connection))
          {
            var dbLastVersion = query.ExecuteScalar();
            if (dbLastVersion == DBNull.Value)
              return 0;

            var lastVersion = (int)dbLastVersion;
            return lastVersion;
          }
      }

      if (Interaction.Query("Table app.DbVersionsLog not exist. Create?") == Interaction.QuaryResult.Yes)
        CreateVersionsLog(connectionConfig);

      return 0;
    }

    private static bool VersionsLogExist(ConnectionConfig connectionConfig)
    {
      using (var connection = new SqlConnection(connectionConfig.SqlConnectionBuilder.ConnectionString))
      {
        connection.Open();
        bool result;

        using (var query = new SqlCommand(CHECK_VERSIONS_LOG_EXISTS_QUERY, connection))
          result = query.ExecuteScalar() != DBNull.Value;

        return result;
      }
    }

    private static void CreateVersionsLog(ConnectionConfig connectionConfig)
    {
      using (var connection = new SqlConnection(connectionConfig.SqlConnectionBuilder.ConnectionString))
      {
        connection.Open();

        bool schemaExist;
        using (var query = new SqlCommand(CHECK_SCHEMA_APP_EXISTS_QUERY, connection))
          schemaExist = query.ExecuteScalar() != DBNull.Value;

        if (!schemaExist)
          using (var command = new SqlCommand(CREATE_SCHEMA_COMMAND, connection))
            command.ExecuteNonQuery();

        using (var query = new SqlCommand(CREATE_VERSIONS_LOG_COMMAND, connection))
          query.ExecuteNonQuery();
      }
    }

    private static IEnumerable<UpdateFileEntry> GetUpdateFileEntries()
    {
      var updateFilesPath = ConfigurationManager.AppSettings["UpdateFilesPath"];
      var updateFileRegExpression = new Regex(UPDATE_FILES_SEACH_PATTERN);

      var allFiles = Directory.GetFiles($"./{updateFilesPath}", "*.sql");

      var result = allFiles
        .Where(file => updateFileRegExpression.IsMatch(Path.GetFileName(file)))
        .Select(file => new UpdateFileEntry(
                              file,
                              Path.GetFileNameWithoutExtension(file),
                              int.Parse(Path.GetFileNameWithoutExtension(file).Substring(7, 4))))
        .ToList();


      Interaction.MessageWrite($"Find {result.Count} sql files");
      result.ForEach(f => Interaction.MessageWrite(f.FileName));

      return result;
    }

    private static void SetUpdateToDataBase(ConnectionConfig connectionConfig, IEnumerable<UpdateFileEntry> updateFilesList)
    {
      foreach (var updateFile in updateFilesList.OrderBy(f => f.Version))
      {
        Interaction.MessageWrite($"File {updateFile.FileName} applay:");

        var script = File.ReadAllText(updateFile.FullFileName, Encoding.Unicode);
        var batches = SplitScriptByBathes(script);

        using (var connection = new SqlConnection(connectionConfig.SqlConnectionBuilder.ConnectionString))
        {
          connection.Open();
          var transaction = connection.BeginTransaction(updateFile.FileName); ;

          foreach (var batch in batches)
          {
            Interaction.MessageWrite(batch);

            try
            {
              using (var command = new SqlCommand(batch, connection, transaction))
                command.ExecuteNonQuery();

              Interaction.SuccessMessageWrite("ОK");
            }
            catch
            {
              transaction.Rollback();
              throw;
            }
          }

          if (VersionsLogExist(connectionConfig))
          {
            using (var command = new SqlCommand(SAVE_ABOUT_VERSION_COMMAND, connection, transaction))
            {
              command.Parameters.Add("@Version", SqlDbType.Int);
              command.Parameters.Add("@ApplayDate", SqlDbType.DateTime);
              command.Parameters.Add("@FileName", SqlDbType.VarChar, 64);

              command.Parameters["@Version"].Value = updateFile.Version;
              command.Parameters["@ApplayDate"].Value = DateTime.Now;
              command.Parameters["@FileName"].Value = updateFile.FileName;

              command.ExecuteNonQuery();
            }
          }

          transaction.Commit();
        }
      }
    }

    private static IEnumerable<string> SplitScriptByBathes(string script)
    {
      return script.Split(batchSplitter, StringSplitOptions.RemoveEmptyEntries);
    }

    private class UpdateFileEntry
    {
      internal readonly string FullFileName;
      internal readonly string FileName;
      internal readonly int Version;

      internal UpdateFileEntry(string fullFileName, string fileName, int version)
      {
        FullFileName = fullFileName;
        FileName = fileName;
        Version = version;
      }
    }
  }
}
