﻿using System;
using System.Data.SqlClient;
using System.Threading;
using AOPAspects.MethodTrace;
using AspectInjector.Broker;
using ConnectionConfigService;
using Extensions;

namespace DBInitiate
{
    [Aspect(typeof(TraceMethodStartAspect))]
    [Aspect(typeof(TraceMethodExitAspect))]
    internal static class DbService
    {
        internal static void Drop(ConnectionConfig connectionConfig)
        {
            var masterConnectionString = CreateMasterDbConnectionString(connectionConfig);

            using (var connection = new SqlConnection(masterConnectionString))
            {
                connection.Open();

                var querySetSingleUserMode = $"ALTER DATABASE [{connectionConfig.SqlConnectionBuilder.InitialCatalog}] SET SINGLE_USER WITH ROLLBACK IMMEDIATE";
                var queryDbDrop = $"Drop database [{connectionConfig.SqlConnectionBuilder.InitialCatalog}]";

                using (var command = new SqlCommand(querySetSingleUserMode, connection))
                    command.ExecuteNonQuery();

                Interaction.MessageWrite($"Data base [{connectionConfig.SqlConnectionBuilder.InitialCatalog}] single user mode on");

                using (var command = new SqlCommand(queryDbDrop, connection))
                    command.ExecuteNonQuery();

                Interaction.MessageWrite($"Data base [{connectionConfig.SqlConnectionBuilder.InitialCatalog}] droped");

                connection.Close();
            }
        }

        internal static void ActivateDataBase(ConnectionConfig connectionConfig) {
            Thread.Sleep(5000); // Waiting while db activating О_о
        }

        internal static void Create(ConnectionConfig connectionConfig)
        {
            if (connectionConfig.IsLocal)
                CreateLocalDb(connectionConfig);
            else
                CreateServerDb(connectionConfig);
        }

        private static void CreateLocalDb(ConnectionConfig connectionConfig)
        {
            var masterConnection = CreateMasterDbConnectionString(connectionConfig);

            var query = $"CREATE DATABASE [{connectionConfig.LocalDbFilePathCommon}] " +
                        "ON PRIMARY (" +
                        $"NAME = [{connectionConfig.LocalDbLogicalName}], " +
                        $"FILENAME = '{connectionConfig.LocalDbFilePath}') " +
                        "LOG ON (" +
                        $"NAME = [{connectionConfig.LocalLogicalLogName}]," +
                        $"FILENAME = '{connectionConfig.LocalLogFilePath}')";

            using (var connection = new SqlConnection(masterConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand(query, connection);
                sqlCommand.ExecuteNonQuery();
            }
        }

        private static void CreateServerDb(ConnectionConfig connectionConfig)
        {
            var masterConnection = CreateMasterDbConnectionString(connectionConfig);
            var query = $"CREATE DATABASE {connectionConfig.SqlConnectionBuilder.InitialCatalog}";
            using (var connection = new SqlConnection(masterConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand(query, connection);
                sqlCommand.ExecuteNonQuery();
            }
        }

        private static string CreateMasterDbConnectionString(ConnectionConfig connectionConfig)
        {
            var masterConnectionBuilder = new SqlConnectionStringBuilder(connectionConfig.SqlConnectionBuilder.ConnectionString) { InitialCatalog = "master" };
            return masterConnectionBuilder.ConnectionString;
        }

        internal static bool TryConnection(ConnectionConfig connectionConfig)
        {
            try
            {
                using (var connection = new SqlConnection(connectionConfig.SqlConnectionBuilder.ConnectionString))
                {
                    connection.Open();
                    connection.Close();
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        private static bool TryMasterConnection(ConnectionConfig connectionConfig)
        {
            var masterConnection = CreateMasterDbConnectionString(connectionConfig);
            try
            {
                using (var connection = new SqlConnection(masterConnection))
                {
                    connection.Open();
                    connection.Close();
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        internal static void TestMasterConnection(ConnectionConfig connectionConfig)
        {
            if (!TryMasterConnection(connectionConfig))
                throw new ApplicationException(DbInitiatiateState.DbmsNotExist.Description(connectionConfig.SqlConnectionBuilder.DataSource));
        }

        internal static bool MasterHasDbEntry(ConnectionConfig connectionConfig)
        {
            var masterConnection = CreateMasterDbConnectionString(connectionConfig);
            try
            {
                using (var connection = new SqlConnection(masterConnection))
                {
                    connection.Open();

                    var query = "SELECT name " +
                                "FROM master.dbo.sysdatabases " +
                                $"WHERE '[' + name + ']' = '{connectionConfig.SqlConnectionBuilder.InitialCatalog}' OR " +
                                $"name = '{connectionConfig.SqlConnectionBuilder.InitialCatalog}'";

                    using (var command = new SqlCommand(query, connection))
                    {
                        var result = command.ExecuteScalar();

                        return !result.IsNull();
                    }
                }
            }
            catch
            {
                return false;
            }
        }
    }
}
