﻿using System;
using System.Diagnostics;
using AOPAspects.MethodTrace;

namespace AOPAspects
{
  public static class AopAspectConfigure
  {
    [Conditional("DEBUG")]
    public static void Configure(Action<string> evMethodStart, Action<string> evMethodExit)
    {
      TraceMethodStartAspect.EvMethodStart += evMethodStart;
      TraceMethodExitAspect.EvMethodExit += evMethodExit;
    }
  }
}
