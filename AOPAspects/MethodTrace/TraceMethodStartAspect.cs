﻿using System;
using System.Diagnostics;
using AspectInjector.Broker;

namespace AOPAspects.MethodTrace
{
    public class TraceMethodStartAspect
    {
        public static event Action<string> EvMethodStart = msg => { };

        [DebuggerStepThrough]
        [Advice(InjectionPoints.Before, InjectionTargets.Method)]
        public void Trace([AdviceArgument(AdviceArgumentSource.TargetName)] string methodName, [AdviceArgument(AdviceArgumentSource.Type)] Type classType)
        {
            EvMethodStart($"Method [{classType.FullName}.{methodName}] enter");
        }
    }
}
