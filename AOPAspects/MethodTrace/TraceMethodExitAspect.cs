﻿using System;
using AspectInjector.Broker;

namespace AOPAspects.MethodTrace
{
    public class TraceMethodExitAspect
    {
        public static event Action<string> EvMethodExit = msg => { };

        [Advice(InjectionPoints.After, InjectionTargets.Method)]
        public void Trace([AdviceArgument(AdviceArgumentSource.TargetName)] string methodName, [AdviceArgument(AdviceArgumentSource.Type)] Type classType)
        {
            EvMethodExit($"Method [{classType.FullName}.{methodName}] exit");
        }
    }
}
