﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace Extensions
{
    public static class EnumExtension
    {
        public static string Description(this Enum value, params object[] insertMessages)
        {
            if (value.GetType().GetCustomAttributes<FlagsAttribute>().Any())
                return string.Empty;

            FieldInfo field = value.GetType().GetField(value.ToString());
            DescriptionAttribute[] attributes = (DescriptionAttribute[])field.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (attributes.Length > 0)
                return string.Format(attributes[0].Description, insertMessages);

            return string.Format(value.ToString(), insertMessages);
        }
    }
}
