﻿using System.ComponentModel;

namespace ConnectionConfigService
{
    public enum ConnectionConfigState
    {
        [Description("All checks passed")]
        Success,
        [Description("Connection name not set")]
        ConnectionNameNotSet,
        [Description("Connection string named '{0}' cant find")]
        ConnectionStringNotExist,
        [Description("Connection string '{0}' is not valid")]
        ConnectionStringIsNotValid,
        [Description("DBMS DataSource (ServerAdress) {0} is not valid")]
        DataSourceNotValid,
        [Description("Invalid localDb name '{0}'")]
        InvalidLocalDbName
    }
}
