﻿using System.IO;

namespace ConnectionConfigService
{
    internal static class FileSystemService {
        internal static bool TryGetFullPathWithoutExtension(string path, out string fullPath) {
            fullPath = string.Empty;

            try {
                fullPath = Path.GetFullPath(Path.Combine(Path.GetDirectoryName(path), Path.GetFileNameWithoutExtension(path)));
                return true;
            }
            catch {
                return false;
            }
        }
    }
}
