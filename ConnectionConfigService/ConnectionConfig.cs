﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using AOPAspects.MethodTrace;
using AspectInjector.Broker;

namespace ConnectionConfigService {
    [Aspect(typeof (TraceMethodStartAspect))]
    [Aspect(typeof (TraceMethodExitAspect))]
    public sealed class ConnectionConfig {
        private readonly List<string> localMarkers;

        public readonly SqlConnectionStringBuilder SqlConnectionBuilder = new SqlConnectionStringBuilder();
        public string ConnectionStringName = string.Empty;

        public bool IsLocal => localMarkers.Any(m => SqlConnectionBuilder.DataSource.ToUpper().Contains(m.ToUpper()));
        public string LocalDbTargetLocalPath => Path.GetPathRoot(LocalDbFilePathCommon);

        public string LocalDbFilePath => $"{LocalDbFilePathCommon}.mdf";
        public string LocalLogFilePath => $"{LocalDbFilePathCommon}.ldf";

        public string LocalDbLogicalName => $"{LocalDbFilePathCommon}_data";
        public string LocalLogicalLogName => $"{LocalDbFilePathCommon}_log";

        public string LocalDbFilePathCommon;

        public ConnectionConfig() {
            localMarkers = new List<string> {".", "localhost", "(local)", "(LocalDb)", "127.0.0.1"};
            localMarkers.AddRange(GetLocalIps());
        }

        private IEnumerable<string> GetLocalIps() {
            return
                Dns.GetHostEntry(Dns.GetHostName()).
                    AddressList.Where(ip => ip.AddressFamily == AddressFamily.InterNetwork).
                    Select(ip => ip.ToString());
        }
    }
}
