﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using AOPAspects.MethodTrace;
using AspectInjector.Broker;
using Extensions;

namespace ConnectionConfigService
{
    [Aspect(typeof(TraceMethodStartAspect))]
    [Aspect(typeof(TraceMethodExitAspect))]
    public static class ConnectionConfigBuilder
    {
        private static ConnectionConfig connectionConfig;

        public static ConnectionConfig Build()
        {
            connectionConfig = new ConnectionConfig();

            ConnectionStringNameBuild();
            var connectionString = ConnectionStringBuild();

            ConnectionBuilderBuild(connectionString);

            if (connectionConfig.IsLocal)
                CorrectionLocalDbName();

            return connectionConfig;
        }

        private static void ConnectionStringNameBuild()
        {
            var result = ConfigurationManager.AppSettings["ConnectionName"];

            if (string.IsNullOrEmpty(result))
                throw new ApplicationException(ConnectionConfigState.ConnectionNameNotSet.Description());

            connectionConfig.ConnectionStringName = result;
        }

        private static string ConnectionStringBuild()
        {
            if (ConfigurationManager.ConnectionStrings[connectionConfig.ConnectionStringName] == null)
                throw new ApplicationException(ConnectionConfigState.ConnectionStringNotExist.Description(connectionConfig.ConnectionStringName));

            return ConfigurationManager.ConnectionStrings[connectionConfig.ConnectionStringName].ConnectionString;
        }

        private static void ConnectionBuilderBuild(string connectionString)
        {
            try
            {
                connectionConfig.SqlConnectionBuilder.ConnectionString = connectionString;
            }
            catch
            {
                throw new Exception(ConnectionConfigState.ConnectionStringIsNotValid.Description(connectionString));
            }
        }

        private static void CorrectionLocalDbName() {
            var initialCatalog =
                string.Concat(
                connectionConfig.SqlConnectionBuilder.InitialCatalog.ToCharArray()
                    .Where((ch, index) =>
                            !(index == 0 && ch == '[') &&
                            !(index == connectionConfig.SqlConnectionBuilder.InitialCatalog.Length - 1 && ch == ']')));

            if (!FileSystemService.TryGetFullPathWithoutExtension(initialCatalog, out connectionConfig.LocalDbFilePathCommon))
                throw new ApplicationException(ConnectionConfigState.InvalidLocalDbName.Description(connectionConfig.SqlConnectionBuilder.InitialCatalog));

            connectionConfig.SqlConnectionBuilder.InitialCatalog = $"{connectionConfig.LocalDbFilePathCommon}";
        }
    }
}

